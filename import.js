var require = {
	js: function(version, localModule){
		var script = document.createElement('script');
		script.src = 'http://requirejs.org/docs/release/' + version + '/minified/require.js';
		if(typeof localModule !== "undefined") {  
			var dataMain = document.createAttribute('data-main');
			dataMain.value = localModule;
			script.setAttributeNode(dataMain);
		} else { console.log('you chose to not load a localModule') }
		var head = document.getElementsByTagName('head')[0];
		head.appendChild(script);
	}
}

require.js('2.1.21','js/app');