require(['toolBox'],function(){
	(function(){
		test("Async Test #1", function() {
			paused();
			setTimeout(function() {
				assert(true, "First test completed");
				resume();
			}, 1000);
		});
		test("Async Test #2", function() {
			paused();
			setTimeout(function() {
				assert(false, "Second test completed");
				resume();
			}, 1000);
		});
		test("Async Test #3", function() {
			paused();
			setTimeout(function() {
				assert(false, "Third test completed");
				resume();
			}, 1000);
		});
		test("Async Test #4", function() {
			paused();
			setTimeout(function() {
				assert(true, "Fourth test completed");
				resume();
			}, 1000);
		});
	})();
});
